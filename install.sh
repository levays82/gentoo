#!/bin/bash

# mount


cd /mnt
wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20210324T214503Z/stage3-amd64-20210324T214503Z.tar.xz

tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner

nano -w /mnt/etc/portage/make.conf

mkdir --parents /mnt/etc/portage/repos.conf
cp /mnt/usr/share/portage/config/repos.conf /mnt/etc/portage/repos.conf/gentoo.conf
nano /mnt/etc/portage/repos.conf/gentoo.conf

cp --dereference /etc/resolv.conf /mnt/etc/

mount --types proc /proc /mnt/proc
mount --rbind /sys /mnt/sys
mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev
mount --make-rslave /mnt/dev 
mount --rbind /run /mnt/run
mount --make-rslave /mnt/run
test -L /dev/shm && rm /dev/shm && mkdir /dev/shm
mount --types tmpfs --options nosuid,nodev,noexec shm /dev/shm
chmod 1777 /dev/shm

chroot /mnt /bin/bash 

source /etc/profile
export PS1="(chroot) ${PS1}"

emerge-webrsync
emerge --sync

emerge --ask --verbose --update --deep --newuse @world
emerge --update --deep --with-bdeps=y --newuse @world

echo "Europe/Kiev" > /etc/timezone
emerge --config sys-libs/timezone-data

nano -w /etc/locale.gen
en_US.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
uk_UA.UTF-8 UTF-8
locale-gen
eselect locale list

nano -w /etc/conf.d/keymaps
nano -w /etc/conf.d/consolefont

rc-update add keymaps boot
rc-update add consolefont boot

env-update && source /etc/profile && export PS1="(chroot) ${PS1}"

nano -w /etc/conf.d/hostname


nano /etc/conf.d/net
config_enp2s0f0="dhcp"

cd /etc/init.d
ln -s net.lo net.enp2s0f0 
rc-update add net.enp2s0f0 default

nano -w /etc/hosts
127.0.1.1     gentoo.homenetwork gentoo localhost

emerge --ask app-admin/sysklogd
rc-update add sysklogd default

emerge --ask net-misc/chrony
rc-update add chronyd default

emerge --ask sys-apps/mlocate sys-fs/dosfstools net-misc/dhcpcd
!!!! emerge sys-apps/irqbalance
emerge --ask dev-util/ccache

###make.conf
FEATURES="ccache"
CCACHE_DIR="/var/cache/ccache"

mkdir /var/cache/ccache/
nano /var/cache/ccache/ccache.conf

# Maximum cache size to maintain
max_size = 30.0G

# Allow others to run 'ebuild' and share the cache.
umask = 002

# Don't include the current directory when calculating
# hashes for the cache. This allows re-use of the cache
# across different package versions, at the cost of
# slightly incorrect paths in debugging info.
# https://ccache.dev/manual/4.4.html#_performance
hash_dir = false

# Preserve cache across GCC rebuilds and
# introspect GCC changes through GCC wrapper.
compiler_check = %compiler% -v

# I expect 1.5M files. 300 files per directory.
cache_dir_levels = 3

###проверка

CCACHE_DIR=/var/cache/ccache ccache -s

###KERNEL
emerge --ask sys-kernel/linux-firmware
echo "sys-firmware/intel-microcode initramfs" >> /etc/portage/package.use/system
emerge --ask --noreplace sys-firmware/intel-microcode
emerge --ask sys-kernel/gentoo-kernel-bin

emerge --ask --verbose --newuse sys-boot/grub:2 sys-boot/os-prober
GRUB_DISABLE_OS_PROBER=false
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GENTOO
grub-mkconfig -o /boot/grub/grub.cfg

nano /etc/security/passwdqc.conf
min=0,0,0,0,0
max=40
passphrase=0
match=0
similar=permit
random=0 
enforce=none
retry=3

useradd -m -G users,wheel,audio -s /bin/bash serg

passwd
passwd serg

rm /stage3-*.tar.*

emerge --ask dev-vcs/git app-shells/zsh app-shells/zsh-completions app-shells/gentoo-zsh-completions eix htop app-misc/mc app-admin/sudo app-portage/gentoolkit

nano /etc/portage/package.use/squashfs
sys-fs/squashfs-tools debug lz4 lzma lzo xattr zstd 

emerge --ask --changed-use --deep sys-fs/squashfs-tools
###ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting

nano ~/.zshrc
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

zstyle ':completion::complete:*' use-cache 1

source /etc/profile 

###EIX

mkdir /etc/eixrc/
nano /etc/eixrc/01-cache

OVERLAY_CACHE_METHOD="assign"

mkdir --parents /etc/portage/postsync.d
nano /etc/portage/postsync.d/eix

#!/usr/bin/env bash
if [[ -e /var/cache/eix/portage.eix ]]; then
    rsync -ca /var/cache/eix/portage.eix /var/cache/eix/previous.eix;
fi
eix-update
if [[ -e /var/cache/eix/previous.eix ]]; then
    eix-diff;
fi

chmod +x /etc/portage/postsync.d/eix

mkdir /etc/portage/repo.postsync.d/
nano /etc/portage/repo.postsync.d/egencache

#!/bin/sh

# The repository name.
repository_name=${1}
# The URI to which the repository was synced.
sync_uri=${2}
# The path to the repository.
repository_path=${3}

# Portage assumes that a hook succeeded if it exits with 0 code. If no
# explicit exit is done, the exit code is the exit code of last spawned
# command. Since our script is a bit more complex, we want to control
# the exit code explicitly.
ret=0

if [ -n "${repository_name}" ]; then
        # Repository name was provided, so we're in a post-repository hook.
        echo "* In post-repository hook for ${repository_name}"
        echo "** synced from remote repository ${sync_uri}"
        echo "** synced into ${repository_path}"

        # Gentoo comes with pregenerated cache but the other repositories
        # usually don't. Generate them to improve performance.
        if [ ! -d "${repository_path}/metadata/md5-cache" ]; then
                if ! egencache --update --repo="${repository_name}" --jobs=$(nproc)
                then
                        echo "!!! egencache failed!"
                        ret=1
                fi
        fi
fi

# Return explicit status.
exit "${ret}"

chmod +x /etc/portage/repo.postsync.d/egencache

eix-sync

###SWAP###

dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile	none	swap	defaults	0 0' >> /etc/fstab

emerge --ask app-portage/genlop
watch -cn 5 genlop -ci

emerge --depclean && eclean-dist --deep && eclean-pkg --deep

emerge --ask --verbose --update --deep --newuse @world



